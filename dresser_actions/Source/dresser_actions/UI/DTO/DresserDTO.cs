﻿using dresser_actions.UI.DTO.SelectionWidgetDTOs;
using dresser_actions.UI.Enums;
using Verse;
using dresser_actions.UI.Util;
using System.Collections.Generic;

namespace dresser_actions.UI.DTO
{
    class DresserDTO
    {
        public bool HasHair { get; protected set; }

        protected long originalAgeBioTicks = long.MinValue;
        protected long originalAgeChronTicks = long.MinValue;

        public readonly Pawn Pawn;
        public CurrentEditorEnum CurrentEditorEnum { get; set; }

        public EditorTypeSelectionDTO EditorTypeSelectionDto { get; protected set; }

        public BodyTypeSelectionDTO BodyTypeSelectionDto { get; protected set; }
        public HairStyleSelectionDTO HairStyleSelectionDto { get; protected set; }
        public HairColorSelectionDTO HairColorSelectionDto { get; protected set; }
        public SliderWidgetDTO SkinColorSliderDto { get; protected set; }
        public HeadTypeSelectionDTO HeadTypeSelectionDto { get; protected set; }

        public DresserDTO(Pawn pawn, CurrentEditorEnum currentEditorEnum, IEnumerable<CurrentEditorEnum> editors)
        {
            this.Pawn = pawn;

            this.CurrentEditorEnum = currentEditorEnum;
            this.EditorTypeSelectionDto = new EditorTypeSelectionDTO(this.CurrentEditorEnum, new List<CurrentEditorEnum>(editors));
            this.EditorTypeSelectionDto.SelectionChangeListener += delegate (object sender)
            {
                this.CurrentEditorEnum = (CurrentEditorEnum)this.EditorTypeSelectionDto.SelectedItem;
                if (this.CurrentEditorEnum == CurrentEditorEnum.ChangeDresserHair)
                {
                    Prefs.HatsOnlyOnMap = true;
                }
                else
                {
                    Prefs.HatsOnlyOnMap = false;
                }
            };
            
            this.HasHair = true;

            this.BodyTypeSelectionDto = null;
            this.HairStyleSelectionDto = null;
            this.HairColorSelectionDto = null;
            this.SkinColorSliderDto = null;
            this.HeadTypeSelectionDto = null;

            this.Initialize();
        }

        protected virtual void Initialize()
        {
#if ALIEN_DEBUG
            Log.Warning("DresserDTO.initialize - start");
#endif
            if (this.EditorTypeSelectionDto.Contains(CurrentEditorEnum.ChangeDresserBody))
            {
                this.originalAgeBioTicks = this.Pawn.ageTracker.AgeBiologicalTicks;
                this.originalAgeChronTicks = this.Pawn.ageTracker.AgeChronologicalTicks;

                this.BodyTypeSelectionDto = new BodyTypeSelectionDTO(this.Pawn.story.bodyType, this.Pawn.gender);

                this.HeadTypeSelectionDto = new HeadTypeSelectionDTO(this.Pawn.story.HeadGraphicPath, this.Pawn.gender);

                this.SkinColorSliderDto = new SliderWidgetDTO(this.Pawn.story.melanin, 0, 1);
            }

            if (this.EditorTypeSelectionDto.Contains(CurrentEditorEnum.ChangeDresserHair))
            {
                this.HairStyleSelectionDto = new HairStyleSelectionDTO(this.Pawn.story.hairDef, this.Pawn.gender);
                this.HairColorSelectionDto = new HairColorSelectionDTO(this.Pawn.story.hairColor);
            }
        }

        public void SetUpdatePawnListeners(UpdatePawnListener updatePawn)
        {
            if (this.BodyTypeSelectionDto != null)
                this.BodyTypeSelectionDto.UpdatePawnListener += updatePawn;
            if (this.HairStyleSelectionDto != null)
                this.HairStyleSelectionDto.UpdatePawnListener += updatePawn;
            if (this.HairColorSelectionDto != null)
                this.HairColorSelectionDto.UpdatePawnListener += updatePawn;
            if (this.SkinColorSliderDto != null)
                this.SkinColorSliderDto.UpdatePawnListener += updatePawn;
            if (this.HeadTypeSelectionDto != null)
                this.HeadTypeSelectionDto.UpdatePawnListener += updatePawn;
        }

        public void ResetToDefault()
        {
#if TRACE
            Log.Warning(System.Environment.NewLine + "DresserDTO.Begin ResetToDefault");
#endif
            if (this.BodyTypeSelectionDto != null)
                this.BodyTypeSelectionDto.ResetToDefault();
            if (this.HairStyleSelectionDto != null)
                this.HairStyleSelectionDto.ResetToDefault();
            if (this.HairColorSelectionDto != null)
                this.HairColorSelectionDto.ResetToDefault();
            if (this.SkinColorSliderDto != null)
                this.SkinColorSliderDto.ResetToDefault();
            if (this.HeadTypeSelectionDto != null)
                this.HeadTypeSelectionDto.ResetToDefault();

            if (this.originalAgeBioTicks != long.MinValue)
                this.Pawn.ageTracker.AgeBiologicalTicks = this.originalAgeBioTicks;
            if (this.originalAgeChronTicks != long.MinValue)
                this.Pawn.ageTracker.AgeChronologicalTicks = this.originalAgeChronTicks;
#if TRACE
            Log.Warning("End DresserDTO.ResetToDefault" + System.Environment.NewLine);
#endif
        }
    }
}
