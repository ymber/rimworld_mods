﻿using dresser_actions.UI.Enums;
using System.Collections.Generic;
using Verse;
using Verse.AI;

namespace dresser_actions.UI.DTO
{
    static class DresserDtoFactory
    {
        public static DresserDTO Create(Pawn pawn, Job job, CurrentEditorEnum selectedEditor)
        {
            IEnumerable<CurrentEditorEnum> editors;
            editors = Building_Dresser.GetSupportedEditors();
            return new DresserDTO(pawn, selectedEditor, editors);
        }
    }
}
