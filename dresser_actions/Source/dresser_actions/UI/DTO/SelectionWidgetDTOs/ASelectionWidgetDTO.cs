﻿using dresser_actions.UI.Util;

namespace dresser_actions.UI.DTO.SelectionWidgetDTOs
{
    abstract class ASelectionWidgetDTO
    {
        protected int index = 0;

        public event SelectionChangeListener SelectionChangeListener;
        public event UpdatePawnListener UpdatePawnListener;

        public void IncreaseIndex()
        {
            ++this.index;
            if (this.index >= this.Count)
                this.index = 0;
            this.IndexChanged();
        }

        public void DecreaseIndex()
        {
            --this.index;
            if (this.index < 0)
                this.index = this.Count - 1;
            this.IndexChanged();
        }

        public abstract int Count { get; }

        public abstract string SelectedItemLabel { get; }

        public abstract object SelectedItem { get; }

        protected void IndexChanged()
        {
            if (this.SelectionChangeListener != null)
                this.SelectionChangeListener.Invoke(this);
            this.UpdatePawn(this.SelectedItem);
        }

        protected void UpdatePawn(object item)
        {
            if (this.UpdatePawnListener != null)
                this.UpdatePawnListener.Invoke(this, item);
        }

        public abstract void ResetToDefault();
    }
}
