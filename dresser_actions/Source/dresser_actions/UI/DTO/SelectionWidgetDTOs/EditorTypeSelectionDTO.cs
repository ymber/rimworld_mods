﻿using dresser_actions.UI.Enums;
using System.Collections.Generic;
using Verse;

namespace dresser_actions.UI.DTO.SelectionWidgetDTOs
{
    class EditorTypeSelectionDTO : ASelectionWidgetDTO
    {
        private List<CurrentEditorEnum> editors;
        public EditorTypeSelectionDTO(CurrentEditorEnum currentEditor, List<CurrentEditorEnum> editors) : base()
        {
            this.editors = new List<CurrentEditorEnum>(editors);

            this.SetSelectedEditor(currentEditor);
        }

        public override int Count
        {
            get
            {
                return this.editors.Count;
            }
        }

        public override string SelectedItemLabel
        {
            get
            {
                return this.editors[base.index].ToString().Translate();
            }
        }

        public override object SelectedItem
        {
            get
            {
                return this.editors[base.index];
            }
        }

        public override void ResetToDefault()
        {
            // Do nothing
        }

        internal bool Contains(CurrentEditorEnum currentEditorEnum)
        {
            return this.editors.Contains(currentEditorEnum);
        }

        internal void Remove(params CurrentEditorEnum[] toRemove)
        {
            foreach (CurrentEditorEnum ed in toRemove)
            {
                this.editors.Remove(ed);
            }
            base.index = 0;
        }

        public void SetSelectedEditor(CurrentEditorEnum editor)
        {
            base.index = 0;
            for (int i = 0; i < this.editors.Count; ++i)
            {
                if (this.editors[i] == editor)
                {
                    base.index = i;
                    break;
                }
            }
        }
    }
}
