﻿using UnityEngine;

namespace dresser_actions.UI.DTO.SelectionWidgetDTOs
{
    class HairColorSelectionDTO : SelectionColorWidgetDTO
    {
        public HairColorSelectionDTO(Color originalColor) : base(originalColor)
        {
        }

        public new void ResetToDefault()
        {
            base.ResetToDefault();
        }
    }
}
