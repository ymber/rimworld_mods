﻿using System;
using System.Collections.Generic;
using System.Linq;
using Verse;

namespace dresser_actions.UI.DTO.SelectionWidgetDTOs
{
    class HeadTypeSelectionDTO : ASelectionWidgetDTO
    {
        private List<string> headTypes;
        private List<string> maleHeadTypes = new List<string>();
        private List<string> femaleHeadTypes = new List<string>();
        private int savedFemaleIndex = 0;
        private int savedMaleIndex = 0;

        public readonly string OriginalHeadType;

        public HeadTypeSelectionDTO(string headType, Gender gender) : base()
        {
            this.OriginalHeadType = headType;

            this.AddHeadTypesToList("Things/Pawn/Humanlike/Heads/Male", this.maleHeadTypes);
            this.AddHeadTypesToList("Things/Pawn/Humanlike/Heads/Female", this.femaleHeadTypes);

            this.Gender = gender;
            this.FindIndex(headType);
        }

        public HeadTypeSelectionDTO(string headType, Gender gender, List<string> crownTypes) : this(headType, gender)
        {
            this.AddHeadTypesToList("Things/Pawn/Humanlike/Heads/Male", crownTypes);
            this.AddHeadTypesToList("Things/Pawn/Humanlike/Heads/Female", crownTypes);
            
            this.FindIndex(headType);
        }

        public void FindIndex(string headType)
        {
            for (int i = 0; i < this.headTypes.Count; ++i)
            {
                if (this.headTypes[i].Equals(headType))
                {
                    base.index = i;
                    break;
                }
            }
        }

        public Gender Gender
        {
            set
            {
                if (value == Gender.Female)
                {
                    this.savedMaleIndex = base.index;
                    this.headTypes = this.femaleHeadTypes;
                    base.index = savedFemaleIndex;
                }
                else // Male
                {
                    this.savedFemaleIndex = base.index;
                    this.headTypes = this.maleHeadTypes;
                    base.index = savedMaleIndex;
                }
                base.IndexChanged();
            }
        }

        public override int Count
        {
            get
            {
                return this.headTypes.Count;
            }
        }

        public override string SelectedItemLabel
        {
            get
            {
                string[] array = this.headTypes[base.index].Split(new char[] { '_' }, StringSplitOptions.None);
                return array[array.Count<string>() - 2] + ", " + array[array.Count<string>() - 1];
            }
        }

        public override object SelectedItem
        {
            get
            {
                return this.headTypes[base.index];
            }
        }

        private void AddHeadTypesToList(string source, List<string> list)
        {
            foreach (string current in GraphicDatabaseUtility.GraphicNamesInFolder(source))
            {
                string item = source + "/" + current;
                list.Add(item);
            }
        }

        public override void ResetToDefault()
        {
            this.FindIndex(this.OriginalHeadType);
            base.IndexChanged();
        }
    }
}