﻿using dresser_actions.UI.Util;
using UnityEngine;

namespace dresser_actions.UI.DTO.SelectionWidgetDTOs
{
    class SelectionColorWidgetDTO
    {
        public event SelectionChangeListener SelectionChangeListener;
        public event UpdatePawnListener UpdatePawnListener;

        public readonly Color OriginalColor;
        private Color selectedColor;

        public Color SelectedColor
        {
            get { return this.selectedColor; }
            set
            {
                if (!this.selectedColor.Equals(value))
                {
                    this.selectedColor = value;
                    if (this.SelectionChangeListener != null)
                        this.SelectionChangeListener.Invoke(this);
                    if (this.UpdatePawnListener != null)
                        this.UpdatePawnListener.Invoke(this, this.selectedColor);
                }
            }
        }

        public SelectionColorWidgetDTO(Color color)
        {
            this.OriginalColor = color;
            this.selectedColor = color;
        }

        public void ResetToDefault()
        {
            this.selectedColor = this.OriginalColor;
            if (this.UpdatePawnListener != null)
                this.UpdatePawnListener.Invoke(this, this.OriginalColor);
        }
    }
}
