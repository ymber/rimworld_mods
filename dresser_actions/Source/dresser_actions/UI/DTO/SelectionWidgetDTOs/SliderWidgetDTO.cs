﻿using dresser_actions.UI.Util;

namespace dresser_actions.UI.DTO.SelectionWidgetDTOs
{
    class SliderWidgetDTO
    {
        public readonly float OriginalValue;
        public readonly float MinValue;
        public readonly float MaxValue;

        private float selectedValue;

        public event UpdatePawnListener UpdatePawnListener;

        public SliderWidgetDTO(float value, float minValue, float maxValue)
        {
            this.OriginalValue = value;
            this.SelectedValue = value;
            this.MinValue = minValue;
            this.MaxValue = maxValue;
        }
        public float SelectedValue
        {
            get { return this.selectedValue; }
            set
            {
                if (this.selectedValue != value)
                {
                    this.selectedValue = value;
                    if (this.UpdatePawnListener != null)
                        this.UpdatePawnListener.Invoke(this, value);
                }
            }
        }

        public void ResetToDefault()
        {
            this.selectedValue = this.OriginalValue;
            if (this.UpdatePawnListener != null)
                this.UpdatePawnListener.Invoke(this, this.OriginalValue);
        }
    }
}
