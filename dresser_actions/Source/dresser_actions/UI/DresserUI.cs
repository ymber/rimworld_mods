﻿using dresser_actions.UI.DTO;
using dresser_actions.UI.Util;
using RimWorld;
using UnityEngine;
using Verse;
using dresser_actions.UI.Enums;
using dresser_actions.UI.DTO.SelectionWidgetDTOs;
using System.Reflection;
using System;
using System.Collections.Generic;

namespace dresser_actions.UI
{
    internal class DresserUI : Window
    {
        private DresserDTO dresserDto;

        private bool rerenderPawn = false;

        private bool saveChangedOnExit = false;

        private bool originalHatsHideSetting;

        public DresserUI(DresserDTO dresserDto)
        {
            this.closeOnClickedOutside = true;
            this.doCloseButton = false;
            this.doCloseX = true;
            this.absorbInputAroundWindow = true;
            this.forcePause = true;
            this.dresserDto = dresserDto;
            this.dresserDto.SetUpdatePawnListeners(this.UpdatePawn);
            this.dresserDto.EditorTypeSelectionDto.SelectionChangeListener += delegate (object sender)
            {
                this.rerenderPawn = true;
            };
        }

        public override Vector2 InitialSize
        {
            get
            {
                return new Vector2(650f, 600f);
            }
        }

        public override void PostOpen()
        {
            base.PostOpen();
            this.originalHatsHideSetting = Prefs.HatsOnlyOnMap;
        }

        public override void PostClose()
        {
            base.PostClose();
            Prefs.HatsOnlyOnMap = this.originalHatsHideSetting;
            this.dresserDto.Pawn.Drawer.renderer.graphics.ResolveAllGraphics();
            PortraitsCache.SetDirty(this.dresserDto.Pawn);
        }
        
        public override void DoWindowContents(Rect inRect)
        {
            try
            {
                if (this.rerenderPawn)
                {
                    this.dresserDto.Pawn.Drawer.renderer.graphics.ResolveAllGraphics();
                    PortraitsCache.SetDirty(this.dresserDto.Pawn);
                    this.rerenderPawn = false;
                }

                Text.Font = GameFont.Medium;

                Widgets.Label(new Rect(0f, 0f, this.InitialSize.y / 2f + 45f, 50f), "dresser_actions.DresserLabel".Translate());

                float portraitBuffer = 30f;

                Rect portraitRect = WidgetUtil.AddPortraitWidget(portraitBuffer, 150f, this.dresserDto);

                float editorLeft = portraitRect.xMax + portraitBuffer;
                float editorTop = 30f + WidgetUtil.SelectionRowHeight;
                float editorWidth = 325f;

                WidgetUtil.AddSelectorWidget(portraitRect.xMax + portraitBuffer, 10f, editorWidth, null, this.dresserDto.EditorTypeSelectionDto);

                switch ((CurrentEditorEnum)this.dresserDto.EditorTypeSelectionDto.SelectedItem)
                {
                    case CurrentEditorEnum.ChangeDresserBody:
                        bool isShowing = false;
                        float top = editorTop;
                        if (this.dresserDto.BodyTypeSelectionDto != null && this.dresserDto.BodyTypeSelectionDto.Count > 1)
                        {
                            WidgetUtil.AddSelectorWidget(editorLeft, top, editorWidth, "dresser_actions.BodyType".Translate() + ":", this.dresserDto.BodyTypeSelectionDto);
                            top += WidgetUtil.SelectionRowHeight + 20f;
                            isShowing = true;
                        }
                        if (this.dresserDto.HeadTypeSelectionDto != null && this.dresserDto.HeadTypeSelectionDto.Count > 1)
                        {
                            WidgetUtil.AddSelectorWidget(editorLeft, top, editorWidth, "dresser_actions.HeadType".Translate() + ":", this.dresserDto.HeadTypeSelectionDto);
                            top += WidgetUtil.SelectionRowHeight + 20f;
                            isShowing = true;
                        }
                        if (this.dresserDto.SkinColorSliderDto != null)
                        {
                            WidgetUtil.AddSliderWidget(editorLeft, top, editorWidth, "dresser_actions.SkinColor".Translate() + ":", this.dresserDto.SkinColorSliderDto);
                            isShowing = true;
                        }

                        if (!isShowing)
                        {
                            GUI.Label(new Rect(editorLeft, top, editorWidth, 40), "dresser_actions.NoEditableAttributes".Translate());
                        }
                        break;
                    case CurrentEditorEnum.ChangeDresserHair:
                        const float listboxHeight = 250f;
                        if (this.dresserDto.HairStyleSelectionDto != null)
                        {
                            float height = listboxHeight;
                            WidgetUtil.AddListBoxWidget(editorLeft, editorTop, editorWidth, height, "dresser_actions.HairStyle".Translate() + ":", this.dresserDto.HairStyleSelectionDto);
                            WidgetUtil.AddColorSelectorWidget(editorLeft, editorTop + listboxHeight + 10f, editorWidth, this.dresserDto.HairColorSelectionDto);
                        }
                        break;
                }

                Text.Anchor = TextAnchor.MiddleLeft;
                Text.Font = GameFont.Tiny;
                GUI.Label(new Rect(0, 400, 250, 100f), GUI.tooltip);
                Text.Font = GameFont.Medium;
                Text.Anchor = TextAnchor.UpperLeft;

                float xWidth = 150;
                float xBuffer = (this.InitialSize.x - xWidth) / 2;
                Rect bottomButtonsRect = new Rect(editorLeft, this.InitialSize.y - WidgetUtil.SelectionRowHeight - 36, xWidth, WidgetUtil.SelectionRowHeight);
                GUI.BeginGroup(bottomButtonsRect);
                Text.Anchor = TextAnchor.MiddleCenter;
                Text.Font = GameFont.Small;
                GUI.color = Color.white;
                if (Widgets.ButtonText(new Rect(0, 0, 60, WidgetUtil.SelectionRowHeight), "Reset".Translate()))
                {
                    this.ResetToDefault();
                }
                if (Widgets.ButtonText(new Rect(90, 0, 60, WidgetUtil.SelectionRowHeight), "Save".Translate()))
                {
                    this.saveChangedOnExit = true;
                    this.Close();
                }
                GUI.EndGroup();
            }
            catch (Exception e)
            {
                Log.Error(this.GetType().Name + " closed due to: " + e.GetType().Name + " " + e.Message);
                Messages.Message(this.GetType().Name + " closed due to: " + e.GetType().Name + " " + e.Message, MessageTypeDefOf.NegativeEvent);
                base.Close();
            }
            finally
            {
                Text.Anchor = TextAnchor.UpperLeft;
                GUI.color = Color.white;
            }
        }

        private void ResetToDefault()
        {
#if TRACE
            Log.Warning(Environment.NewLine + "Begin DresserUI.ResetToDefault");
#endif

            this.dresserDto.ResetToDefault();
            this.UpdatePawn(null, null);
            
#if TRACE
            Log.Warning("End DresserUI.ResetToDefault" + Environment.NewLine);
#endif
        }

        public override void PreClose()
        {
#if TRACE
            Log.Message(Environment.NewLine + "Start DresserUI.PreClose");
#endif
            try
            {
                base.PreClose();

                if (!this.saveChangedOnExit)
                {
                    this.ResetToDefault();
                }
            }
            catch (Exception e)
            {
                Log.Error("Error on DresserUI.PreClose: " + e.GetType().Name + " " + e.Message + Environment.NewLine + e.StackTrace);
            }
#if TRACE
            Log.Message("End DresserUI.PreClose" + Environment.NewLine);
#endif
        }

        private void UpdatePawn(object sender, object value)
        {
            if (sender != null)
            {
                Pawn pawn = this.dresserDto.Pawn;

                if (sender is BodyTypeSelectionDTO)
                {
                    pawn.story.bodyType = (BodyTypeDef)value;
                }
                else if (sender is HairColorSelectionDTO)
                {
                    pawn.story.hairColor = (Color)value;
                }
                else if (sender is HairStyleSelectionDTO)
                {
                    pawn.story.hairDef = (HairDef)value;
                }
                else if (sender is HeadTypeSelectionDTO)
                {
                    if (value.ToString().IndexOf("Narrow") >= 0 ||
                        value.ToString().IndexOf("narrow") >= 0)
                    {
                        dresserDto.Pawn.story.crownType = CrownType.Narrow;
                    }
                    else
                    {
                        dresserDto.Pawn.story.crownType = CrownType.Average;
                    }
                    typeof(Pawn_StoryTracker).GetField("headGraphicPath", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(dresserDto.Pawn.story, value);
                }
                else if (sender is SliderWidgetDTO)
                {
                    pawn.story.melanin = (float)value;
                }
            }
            rerenderPawn = true;
        }
    }
}
