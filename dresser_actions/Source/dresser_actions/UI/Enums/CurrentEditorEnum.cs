﻿namespace dresser_actions.UI.Enums
{
    public enum CurrentEditorEnum
    {
        ChangeDresserHair,
        ChangeDresserBody,
    }
}
