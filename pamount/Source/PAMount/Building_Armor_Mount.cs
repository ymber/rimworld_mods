﻿using System;
using System.Collections.Generic;

using RimWorld;
using Verse;
using Verse.AI;

namespace PAMount
{
    class Building_Armor_Mount : Building_Storage
    {
        private CompPowerTrader powerComp;

        public override void SpawnSetup(Map map, bool respawningAfterLoad)
        {
            base.SpawnSetup(map, respawningAfterLoad);
            this.powerComp = base.GetComp<CompPowerTrader>();
        }

        private FloatMenuOption GetFailureReason(Pawn myPawn)
        {
            if (!myPawn.CanReach(this, PathEndMode.OnCell, Danger.Some, false, TraverseMode.ByPawn))
            {
                return new FloatMenuOption("CannotUseNoPath".Translate(), null, MenuOptionPriority.Default, null, null, 0f, null, null);
            }
            if (!this.Map.thingGrid.CellContains(this.Position, ThingCategory.Item))
            {
                return new FloatMenuOption("CannotUseNoItem".Translate(), null, MenuOptionPriority.Default, null, null, 0f, null, null);
            }
            if (base.Spawned && base.Map.gameConditionManager.ConditionIsActive(GameConditionDefOf.SolarFlare))
            {
                return new FloatMenuOption("CannotUseSolarFlare".Translate(), null, MenuOptionPriority.Default, null, null, 0f, null, null);
            }
            if (!this.powerComp.PowerOn)
            {
                return new FloatMenuOption("CannotUseNoPower".Translate(), null, MenuOptionPriority.Default, null, null, 0f, null, null);
            }
            return null;
        }

        public override IEnumerable<FloatMenuOption> GetFloatMenuOptions(Pawn myPawn)
        {
            FloatMenuOption failureReason = this.GetFailureReason(myPawn);
            if (failureReason != null)
            {
                yield return failureReason;
                yield break;
            }
            else
            {
                Action action = delegate ()
                {
                    Job wear_armor = new Job(JobDefOf_MountPowerArmor.MountPowerArmor, this.Map.thingGrid.ThingAt(this.Position, ThingCategory.Item));
                    myPawn.jobs.TryTakeOrderedJob(wear_armor);
                    myPawn.Reserve(this, wear_armor);
                };
                FloatMenuOption option = new FloatMenuOption("Wear Power Armor", action);
                if (option != null)
                {
                    yield return option;
                }
            }
            yield break;
        }
    }
}
