﻿using RimWorld;
using Verse;

namespace PAMount
{
    [DefOf]
    public static class JobDefOf_MountPowerArmor
    {
        static JobDefOf_MountPowerArmor()
        {
            DefOfHelper.EnsureInitializedInCtor(typeof(JobDefOf));
        }

        public static JobDef MountPowerArmor;
    }
}
