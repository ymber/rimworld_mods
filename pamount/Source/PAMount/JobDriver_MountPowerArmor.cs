﻿using System.Collections.Generic;

using RimWorld;
using Verse;
using Verse.AI;

namespace PAMount
{
    class JobDriver_MountPowerArmor : JobDriver
    {
        private int duration;
        private int unequipBuffer;

        private Apparel Armor
        {
            get
            {
                return (Apparel)this.job.GetTarget(TargetIndex.A).Thing;
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look<int>(ref this.duration, "duration", 0, false);
            Scribe_Values.Look<int>(ref this.unequipBuffer, "unequipBuffer", 0, false);
        }

        public override bool TryMakePreToilReservations(bool errorOnFailed) => this.pawn.Reserve(this.Armor, this.job, 1, -1, null, errorOnFailed);

        public override void Notify_Starting()
        {
            base.Notify_Starting();
            this.duration = (int)(this.Armor.GetStatValue(StatDefOf.EquipDelay, true) / 6 * 60f);
            Apparel armor = this.Armor;
            List<Apparel> wornApparel = this.pawn.apparel.WornApparel;
            for (int i = wornApparel.Count - 1; i >= 0; i--)
            {
                if (!ApparelUtility.CanWearTogether(armor.def, wornApparel[i].def, this.pawn.RaceProps.body))
                {
                    if (wornApparel[i].def.defName == "Apparel_PowerArmor")
                    {
                        this.duration += (int)(wornApparel[i].GetStatValue(StatDefOf.EquipDelay, true) / 6 * 60f);
                    }
                    else
                    {
                        this.duration += (int)(wornApparel[i].GetStatValue(StatDefOf.EquipDelay, true) * 60f);
                    }
                }
            }
        }

        private void TryUnequipSomething()
        {
            List<Apparel> wornApparel = this.pawn.apparel.WornApparel;
            for (int i = wornApparel.Count - 1; i >= 0; i--)
            {
                if (!ApparelUtility.CanWearTogether(this.Armor.def, wornApparel[i].def, this.pawn.RaceProps.body))
                {
                    int unequip_time;
                    if (wornApparel[i].def.defName == "Apparel_PowerArmor")
                    {
                        unequip_time = (int)(wornApparel[i].GetStatValue(StatDefOf.EquipDelay, true) / 6 * 60f);
                    }
                    else
                    {
                        unequip_time = (int)(wornApparel[i].GetStatValue(StatDefOf.EquipDelay, true) * 60f);
                    }
                    if (this.unequipBuffer >= unequip_time)
                    {
                        bool forbid = this.pawn.Faction != null && this.pawn.Faction.HostileTo(Faction.OfPlayer);
                        Apparel apparel2;
                        if (!this.pawn.apparel.TryDrop(wornApparel[i], out apparel2, this.pawn.PositionHeld, forbid))
                        {
                            Log.Error(this.pawn + " could not drop " + wornApparel[i].ToStringSafe<Apparel>(), false);
                            base.EndJobWith(JobCondition.Errored);
                            return;
                        }
                    }
                    break;
                }
            }
        }

        protected override IEnumerable<Toil> MakeNewToils()
        {
            this.FailOnBurningImmobile(TargetIndex.A);
            yield return Toils_Goto.GotoThing(TargetIndex.A, PathEndMode.OnCell).FailOnDespawnedNullOrForbidden(TargetIndex.A);
            Toil prepare = new Toil();
            prepare.tickAction = delegate ()
            {
                this.unequipBuffer++;
                this.TryUnequipSomething();
            };
            prepare.WithProgressBarToilDelay(TargetIndex.A, false, -0.5f);
            prepare.FailOnDespawnedNullOrForbidden(TargetIndex.A);
            prepare.defaultCompleteMode = ToilCompleteMode.Delay;
            prepare.defaultDuration = this.duration;
            yield return prepare;
            yield return Toils_General.Do(delegate
            {
                this.pawn.apparel.Wear(this.Armor, true);
                if (this.pawn.outfits != null && this.job.playerForced)
                {
                    this.pawn.outfits.forcedHandler.SetForced(this.Armor, true);
                }
            });
            yield break;
        }
    }
}
